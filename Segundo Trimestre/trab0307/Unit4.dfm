object DataModule4: TDataModule4
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Height = 306
  Width = 408
  object FDPhysMySQLDriverLink1: TFDPhysMySQLDriverLink
    VendorLib = 
      'C:\Users\Informatica03\Documents\EasyPHP-DevServer-14.1VC9\binar' +
      'ies\mysql\lib\libmysql.dll'
    Left = 24
    Top = 40
  end
  object FDConnection1: TFDConnection
    Params.Strings = (
      'Database=3_53pokemon'
      'User_Name=root'
      'DriverID=MySQL')
    Connected = True
    Left = 112
    Top = 32
  end
  object queryTreinador: TFDQuery
    Connection = FDConnection1
    SQL.Strings = (
      'SELECT * FROM treinador')
    Left = 192
    Top = 40
  end
  object queryPokemon: TFDQuery
    Connection = FDConnection1
    SQL.Strings = (
      'SELECT * FROM pokemons')
    Left = 192
    Top = 104
  end
  object dataSourceTreinador: TDataSource
    DataSet = queryTreinador
    Left = 296
    Top = 40
  end
  object DataSourcePokemon: TDataSource
    DataSet = queryPokemon
    Left = 288
    Top = 112
  end
end
