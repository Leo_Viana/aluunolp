unit Unit3;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Unit4, Unit5, Vcl.StdCtrls, Vcl.DBCtrls,
  Vcl.ExtCtrls;

type
  TForm3 = class(TForm)
    DBLookupListBox1: TDBLookupListBox;
    DBText1: TDBText;
    DBText2: TDBText;
    DBText3: TDBText;
    DBText4: TDBText;
    Label1: TLabel;
    labelTreinador: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    Panel1: TPanel;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form3: TForm3;
  Form5: TForm5;

implementation

{$R *.dfm}

procedure TForm3.Button1Click(Sender: TObject);
begin
Form5 := TForm5.Create(Application);
Form5.ShowModal;
end;

procedure TForm3.Button2Click(Sender: TObject);
begin
DataModule4.queryPokemon.Delete;
end;

procedure TForm3.Button3Click(Sender: TObject);
begin
Form5 := TForm5.Create(Application);
Form5.ShowModal;
DataModule4.queryPokemon.Append;
end;

end.
