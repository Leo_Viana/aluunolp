object Form3: TForm3
  Left = 0
  Top = 0
  Caption = 'PokeCenter'
  ClientHeight = 378
  ClientWidth = 582
  Color = clMaroon
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -16
  Font.Name = 'Arial'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  PixelsPerInch = 96
  TextHeight = 18
  object DBLookupListBox1: TDBLookupListBox
    Left = 24
    Top = 48
    Width = 233
    Height = 256
    KeyField = 'Identificador'
    ListField = 'nome'
    ListSource = DataModule4.DataSourcePokemon
    TabOrder = 0
  end
  object Button1: TButton
    Left = 352
    Top = 224
    Width = 137
    Height = 25
    Caption = 'Editar Pokemon'
    TabOrder = 1
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 352
    Top = 320
    Width = 137
    Height = 25
    Caption = 'Deletar Pokemon'
    TabOrder = 2
    OnClick = Button2Click
  end
  object Button3: TButton
    Left = 352
    Top = 272
    Width = 137
    Height = 25
    Caption = 'Inserir Pokemon'
    TabOrder = 3
    OnClick = Button3Click
  end
  object Panel1: TPanel
    Left = 277
    Top = 48
    Width = 297
    Height = 170
    Color = clWhite
    ParentBackground = False
    TabOrder = 4
    object DBText1: TDBText
      Left = 136
      Top = 16
      Width = 129
      Height = 17
      DataField = 'Identificador'
      DataSource = DataModule4.DataSourcePokemon
    end
    object DBText2: TDBText
      Left = 136
      Top = 55
      Width = 129
      Height = 17
      DataField = 'Treinador'
      DataSource = DataModule4.DataSourcePokemon
    end
    object DBText3: TDBText
      Left = 136
      Top = 91
      Width = 129
      Height = 17
      DataField = 'Nome'
      DataSource = DataModule4.DataSourcePokemon
    end
    object DBText4: TDBText
      Left = 136
      Top = 133
      Width = 129
      Height = 17
      DataField = 'Nivel'
      DataSource = DataModule4.DataSourcePokemon
    end
    object Label1: TLabel
      Left = 8
      Top = 16
      Width = 85
      Height = 18
      Caption = 'Identificador'
    end
    object Label3: TLabel
      Left = 8
      Top = 91
      Width = 42
      Height = 18
      Caption = 'Nome'
    end
    object Label4: TLabel
      Left = 8
      Top = 132
      Width = 34
      Height = 18
      Caption = 'Nivel'
    end
    object labelTreinador: TLabel
      Left = 7
      Top = 55
      Width = 66
      Height = 18
      Caption = 'Treinador'
    end
  end
end
